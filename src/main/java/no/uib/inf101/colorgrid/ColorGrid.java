package no.uib.inf101.colorgrid;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{

	final int rows;
	final int cols;
	List <List <CellColor>> grid;

	public ColorGrid(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		grid = new ArrayList<>();
		for (int i = 0; i < rows; i++) {
			List <CellColor> tempList = new ArrayList<>();
			for (int j = 0; j < cols; j++) {
				CellPosition pos = new CellPosition(i, j);
				CellColor cell = new CellColor(pos, null);
				tempList.add(cell);
			}
			grid.add(tempList);
		}
	}

    /**
   	* Get a list containing the GridCell objects in this collection
   	*
   	* @return a list of all GridCell objects in this collection
   	*/
  	public List<CellColor> getCells() {
		List <CellColor> tempGrid = new ArrayList<>();
		grid.forEach(tempGrid::addAll);
		return tempGrid;
	}

  	/** Number of rows in the grid. */
 	public int rows() {
		return rows;
	}

    /** Number of columns in the grid. */
    public int cols() {
		return cols;
	}

  	/**
   	* Get the color of the cell at the given position.
	*
	* @param pos the position
	* @return the color of the cell
	* @throws IndexOutOfBoundsException if the position is out of bounds
	*/
	public Color get(CellPosition pos) {
		int row = pos.row();
		int col = pos.col();
		CellColor cell = grid.get(row).get(col);
		return cell.color();
	}

	/**
	 * Set the color of the cell at the given position.
	 *
	 * @param pos the position
	 * @param color the new color
	 * @throws IndexOutOfBoundsException if the position is out of bounds
	 */
	public void set(CellPosition pos, Color color) {
		int row = pos.row();
		int col = pos.col();
		CellColor cell = new CellColor(pos, color);
		grid.get(row).set(col, cell);
	}
}
