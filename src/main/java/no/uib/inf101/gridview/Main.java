package no.uib.inf101.gridview;

import javax.swing.JFrame;
import java.awt.Color;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {

		ColorGrid grid = new ColorGrid(3, 4);
		grid.set(new CellPosition(0, 0), new Color(255, 0, 0));
		grid.set(new CellPosition(0, 3), new Color(0, 0, 255));
		grid.set(new CellPosition(2, 0), new Color(255, 255, 0));
		grid.set(new CellPosition(2, 3), new Color(0, 255, 0));
		GridView graph = new GridView(grid);
		JFrame frame = new JFrame();
		frame.setContentPane(graph);
		frame.setTitle("INF101");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
  }
}
