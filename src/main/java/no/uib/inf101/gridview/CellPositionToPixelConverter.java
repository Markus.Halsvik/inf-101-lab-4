package no.uib.inf101.gridview;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
	Rectangle2D box;
	GridDimension gd;
	double margin;
	
	public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
		this.box = box;
		this.gd = gd;
		this.margin = margin;
	}

	public Rectangle2D getBoundsForCell(CellPosition pos) {
		int rows = gd.rows();
		int cols = gd.cols();
		int row = pos.row();
		int col = pos.col();
		double width = box.getBounds().getWidth();
		double height = box.getBounds().getHeight();
		double cellWidth = (width - (cols + 1) * margin) / cols;
		double cellHeight = (height - (rows + 1) * margin) / rows;
		double cellx = box.getX() + margin + (cellWidth + margin) * col;
		double celly = box.getY() + margin + (cellHeight + margin) * row;
		Rectangle2D.Double cell = new Rectangle2D.Double(cellx, celly, cellWidth, cellHeight);
		return cell;
	}
}
