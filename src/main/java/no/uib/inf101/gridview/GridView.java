package no.uib.inf101.gridview;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
	IColorGrid grid;
	private static final double OUTERMARGIN = 30;
	private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  
	public GridView(IColorGrid grid) {
		this.setPreferredSize(new Dimension(400, 300));
		this.grid = grid;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graph = (Graphics2D) g;
		drawGrid(graph);
	}

	private void drawGrid(Graphics2D graph) {
		double x = OUTERMARGIN;
		double y = OUTERMARGIN;
		double width = this.getWidth() - 2 * OUTERMARGIN;
		double height = this.getHeight() - 2 * OUTERMARGIN;
		Rectangle2D box = new Rectangle2D.Double(x, y, width, height);
		graph.setColor(MARGINCOLOR);
		graph.fill(box);
		CellPositionToPixelConverter cellConverter = new CellPositionToPixelConverter(box, grid, OUTERMARGIN);
		drawCells(graph, grid, cellConverter);
	}

	static private void drawCells(Graphics2D graph, CellColorCollection cells, CellPositionToPixelConverter cellConverter) {
		for (CellColor cell : cells.getCells()) {
			CellPosition pos = cell.cellPosition();
			Rectangle2D cellBox = cellConverter.getBoundsForCell(pos);
			Color color = cell.color();
			if (color == null) {
				graph.setColor(Color.DARK_GRAY);
			}
			else {
				graph.setColor(color);
			}
			graph.fill(cellBox);
		}
	}
}
